# production
FROM amazoncorretto:11

EXPOSE 8080

RUN mkdir /app
COPY  ./gci-simple/target/*.jar /app/app.jar

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app/app.jar"]
