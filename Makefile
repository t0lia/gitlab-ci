OS=linux
ARCH=amd64

build:
	docker build -t t0lia/gitlab-ci .

run:
	docker run -d --name gci-app -p 8080:8080 t0lia/gitlab-ci