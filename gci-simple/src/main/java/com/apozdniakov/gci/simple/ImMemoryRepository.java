package com.apozdniakov.gci.simple;

import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class ImMemoryRepository {

    Map<Long, String> map = new HashMap<>();

    @EventListener
    public void onStart(ContextRefreshedEvent e) {
        map.put(1L, "fst");
    }

    public void put(long id, String value) {
        map.put(id, value);
    }

    public String get(long id) {
        return map.get(id);
    }
}
