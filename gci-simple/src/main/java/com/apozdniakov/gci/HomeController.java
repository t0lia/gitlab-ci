package com.apozdniakov.gci;

import com.apozdniakov.gci.simple.ImMemoryRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

    private ImMemoryRepository imMemoryRepository;

    public HomeController(ImMemoryRepository imMemoryRepository) {
        this.imMemoryRepository = imMemoryRepository;
    }

    @GetMapping("/")
    public String hello() {
        return imMemoryRepository.get(1L);
    }

}
