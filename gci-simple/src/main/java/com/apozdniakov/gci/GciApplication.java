package com.apozdniakov.gci;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GciApplication {

	public static void main(String[] args) {
		SpringApplication.run(GciApplication.class, args);
	}

}
