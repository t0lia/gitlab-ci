package com.apozdniakov.gci.simple;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ImMemoryRepositoryTest {

    @Test
    public void test() {
        ImMemoryRepository repo = new ImMemoryRepository();
        repo.put(1L, "fst");
        Assertions.assertEquals("fst", repo.get(1L), "Unit test");
    }
}