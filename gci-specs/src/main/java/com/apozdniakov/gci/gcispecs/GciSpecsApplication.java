package com.apozdniakov.gci.gcispecs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class GciSpecsApplication {

    public static void main(String[] args) {
        SpringApplication.run(GciSpecsApplication.class, args);
    }



}
