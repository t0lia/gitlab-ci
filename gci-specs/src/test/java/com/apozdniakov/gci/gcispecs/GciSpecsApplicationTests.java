package com.apozdniakov.gci.gcispecs;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.web.client.RestTemplate;

@SpringBootTest
class GciSpecsApplicationTests {


    private RestTemplate restTemplate;

    @Autowired
    public GciSpecsApplicationTests(RestTemplateBuilder builder) {
        this.restTemplate = builder.build();
    }


    @Test
    public void restTest() {
        String url = "http://app:8080";
        String result = this.restTemplate.getForObject(url, String.class);
        Assertions.assertEquals("fst", result);
    }

}
