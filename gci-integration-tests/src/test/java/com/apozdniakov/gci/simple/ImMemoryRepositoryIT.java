package com.apozdniakov.gci.simple;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ImMemoryRepositoryIT {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private ImMemoryRepository repo;

    @Test
    public void restTest() {
        String url = "http://localhost:" + port;
        String result = this.restTemplate.getForObject(url, String.class);
        Assertions.assertEquals("fst", result);
    }

    @Test
    public void repoTest() {
        Assertions.assertEquals("fst", repo.get(1L), "It test");
    }
}